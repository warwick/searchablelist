### Searchable List For jQuery v 1.0.0

This library creates an HTML list which can be styled and searched.
The library consists of two files 'searchableList.js', 'searchableList.css' and works with jQuery.
Here is a link to the live demo: http://developersfound.com/SearchableList/

The generated HTML is as follows:
```html
<input class="searchBar" id="txtCategories" placeholder="Type To Search .... " type="text">
<div>
   <div class="customSelect .item">Bonets<input value="5" type="hidden"></div>
   <div class="customSelect .item">Bumpers<input value="3" type="hidden"></div>
   <div class="customSelect .item">Doors<input value="4" type="hidden"></div>
   <div class="customSelect .item">Engines<input value="7" type="hidden"></div>
   <div class="customSelect .item">Hatches<input value="6" type="hidden"></div>
   <div class="customSelect .item">Head Lights<input value="2" type="hidden"></div>
   <div class="customSelect .item">Tail Lights<input value="1" type="hidden"></div>
</div>
```

**The features of this library are:**

1. It is possible to get a handle on all tags.
1. It is possible to have differently styled lists on the same web page (with dynamic programming).
1. The behaviour is intuitive with overridable behaviours.
1. The list is dynamically updated while typing into the search bar.


The following methods are designed to be overridable:

void bindClick(dataArr, target, clickedVal, textVal)

What happens when an item is clicked. Default behaviour is to populate the search bar with selected text.

Parameters: dataArr the data that has been used to populate the list, target is a handle on the clicked div element, clickedVal is the index (integer) of the clicked item and textVal is the text value of the div element.

Void bindKeyUp(inputStr, target)

Method dictates what happens on key up event. Default behaviour is to filter the list.

Parameters: InputStr is the text value of search bar, target is the input field.


**Usage:**

include files 'searchableList.js' and 'searchableList.css'. In header section of HTML file (See file index.html).

Populate the data array. Must be at least 2 x n elements in the following format but can have as many additional elements for tags:

```js
data[n] = [
    [idx],
    [label_text],
    ['Add as many tags'],
    ['as you wish and you can'],
    ['get a handle on them from'],
    ['bindClickSearchable method']
];
```

call method void buildCustomSelect('listContainer', 'Start Typing to Search .... ', 'searchBar', data);

Parameters:

First parameter: The id of the div tag to be dynamically populated with the data structure.

Second parameter: The placeholder for the search bar.

Third parameter: The id to be assigned to the search bar input tag of type text.

Fourth parameter: The data used to generate the list.