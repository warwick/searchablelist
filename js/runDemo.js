/* This file is not part of the Searchable List library but part of the demo and is not licensed. */
function populateList() {
	
	/* Data array must be in the following structure:
    data[n] = [
        [idx],
        [label_text]
    ];
	*/
	
	var philosophies = [[]];
    philosophies[0] = [[1],['A truly humble person can not be humiliated.']];
    philosophies[1] = [[2],['End.']];
    philosophies[2] = [[3],['A Person\'s beliefs and a person\'s nature go hand in hand.']];
    philosophies[3] = [[4],['End.']];
    philosophies[4] = [[5],['If this is true, that is not necessarily false.']];
    philosophies[5] = [[6],['End.']];
    philosophies[6] = [[7],['Let go of all that you are to be all you can be,']];
    philosophies[7] = [[8],['for without identity there is no prejudice or war.']];
    philosophies[8] = [[9],['End.']];
    philosophies[9] = [[10],['Dependence is a prerequisite to control.']];
    philosophies[10] = [[11],['End.']];
    philosophies[11] = [[12],['Start as you mean to continue,']];
    philosophies[12] = [[13],['because you\'ll most likely continue as you start.']];
    philosophies[13] = [[14],['End.']];
    philosophies[14] = [[15],['The mind is indeed powerful, but beliefs are indomitable.']];
    philosophies[15] = [[16],['End.']];
    philosophies[16] = [[17],['Truth is truth no matter whose mouth it comes from.']];
    philosophies[17] = [[18],['End.']];
    philosophies[18] = [[19],['Evil is insidious, the definition being:']];
    philosophies[19] = [[20],['Slow and subtle but with harmful effects.']];
    
    /*
    void buildCustomSelect(divContainerTag, placeHolder, searchBarTextInput, data);
    param 1: Any existing Div tag id in HTML Layout
    param 2: The place holder that will be displayed in the search bar text control
    param 3: The id of the text control. This id will give you the handle to the control should you need it.
    param 4: The data to populate the list must be in the above format.
    */
    buildCustomSelect('listContainer', 'Start Typing to Search .... ', 'searchBar', philosophies);
    
}