 /** js
 * searchableList.js is a jQuery plugin to allow dynamic searching of items
 * The unit is pre-programmed with intuitive default behaviour but is designed to have that behaviour overridden.
 * This builds dynamic HTML in the following format:
 * <input class="searchBar" type="text" id="searchBar" placeholder="Start Typing to Search .... ">
 * <div class="itemContainer">
 *    <div class="item">First Text Item.<input type="hidden" value="1"></div>
 *    <div class="item">Second Text Item<input type="hidden" value="2"></div>
 *    <!-- more divs here etc .... -->
 * </div>
 * This library is also designed so that you can get handles on every tag and potentially style the search objects seperately
 * should you have more than one list on the same web page.
 * @copyright   (c) May 7th 2019, Warwick Weston Wright <warwickwestonwright@live.com>
 * @version     1.0.0
 * @license     BSD
 */
function buildCustomSelect(customSelectDivContainer, placeHolder, searchBarId, dataArr) {
	$('#' + customSelectDivContainer).empty();
	var listContainer = $('#' + customSelectDivContainer)
		.addClass('customSelect');
	var textNode = $('<input>')
		.addClass('searchBar')
		.attr({type: 'text', id: searchBarId, placeholder: placeHolder});
	$(listContainer).append(textNode);
	var itemsContainer = $('<div>').addClass('itemContainer');
	$(listContainer).append(itemsContainer);

	$.each(dataArr, function(index, value) {
		var item = $('<div>').addClass('item');
		var hiddenInput = $('<input>').attr({type: 'hidden', 'value': value[0]});
		$(item).text(value[1]);
		$(item).append(hiddenInput);
		$(itemsContainer).append(item);
	});
	
	$(itemsContainer).children().each(function() {
		if($(this).is('div')) {
			$(this).on('click', function() {
				var inputNode = $(this).children()[0];
				bindClickSearchable(dataArr, this, $(inputNode).val(), $(this).text());
			});
		}
	});
	
	$(textNode).on('keyup', function() {
		bindKeyUpSearchable($(this).val(), this);
	});
}

/*
This method has been pre programmed with click behaviour populate the searcBar with selected item
Feel free to override this method with your desired behaviour.
void bindClickSearchable(dataArr, clickedDiv, clickedIdxVal, clickedTextVal)
*/
function bindClickSearchable(dataArr, target, clickedVal, textVal) {
	//target is the div element of the item
	var txtControl = $(target).parent().parent().children()[0];
	$(txtControl).val(textVal);
	alert('You clicked item index: ' + clickedVal);
}

/*
This method has been pre programmed with behaviour to hide all items that do not contain the text
typed in the search bar and re-reveal them when they do contain that text. Empty search bar will
reveal all items.
Feel free to override this method with your desired behaviour.
void bindKeyUpSearchable(searchInputStr, searchBarControl)
*/
function bindKeyUpSearchable(inputStr, target) {
	//target is input element of the searchBar
	var txtId = $(target).attr('id');
	var parentId = $(target).parent().attr('id');
	$('#' + txtId).val($('#' + txtId).val());
	var itemsContainer = $('#' + parentId).children()[1];
	$(itemsContainer).children().each(function() {
		if($(this).is('div')) {
			var divText = $(this).text();
			if(divText.toLowerCase().indexOf(inputStr.toLowerCase().trim()) > -1 || inputStr == '') {
				$(this).addClass('show');
				$(this).removeClass('hidden');
			}
			else {
				$(this).addClass('hidden');
				$(this).removeClass('show');
			}
		}
	});
}